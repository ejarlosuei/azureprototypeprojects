﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using BusinessLayer;
using BusinessLayer.DTO;
using BusinessLayer.TestData;

namespace EmployeesService
{
    public class EmployeesController : BaseController
    {
        /// <summary>
        /// Retrieves All Employees
        /// </summary>
        /// <returns>HttpResponseMessage with list od Employee DTO objects</returns>
        [Route("employees")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetAllEmployees()
        {
            try

            {
                List<Employee> employeeList = EmployeesData.GetAllEmployees();
                return CreateJsonResponse(Request, employeeList, Globals.MessageCodes.SUCCESS);

            }
            catch (BusinessException bex)
            {
                //LogError("Error at GetAllEmployees", bex);
                return CreateJsonResponse(Request, null, bex.Code);
            }
            catch (Exception ex)
            {
                //LogError("Unhandled exception occurred.", ex);
                return CreateJsonResponse(Request, null, Globals.MessageCodes.ERROR);
            }
        }

        /// <summary>
        /// Retrieves Employee by id.
        /// </summary>
        /// <param name="id">Employee Id</param>
        /// <returns>HttpResponseMessage with Employee DTO object</returns>
        [Route("employees/{id}")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetEmployeeById([FromUri] int id)
        {
            try
            {
                Employee employee = EmployeesData.GetEmployeeById(id);
                return CreateJsonResponse(Request, employee, Globals.MessageCodes.SUCCESS);

            }
            catch (BusinessException bex)
            {
                //LogError("Error at GetEmployeeById", bex);
                return CreateJsonResponse(Request, null, bex.Code);
            }
            catch (Exception ex)
            {
                //LogError("Unhandled exception occurred.", ex);
                return CreateJsonResponse(Request, null, Globals.MessageCodes.ERROR);
            }
        }

        /// <summary>
        /// Adds new employee.
        /// </summary>
        /// <param name="employee">Employee object</param>
        /// <returns>HttpResponseMessage with Employee DTO object</returns>
        [Route("employees")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage AddEmployee([FromBody] Employee employee)
        {
            try
            {
                Employee newEmployee = EmployeesData.AddEmployee(employee);
                return CreateJsonResponse(Request, newEmployee, Globals.MessageCodes.SUCCESS);

            }
            catch (BusinessException bex)
            {
                //LogError("Error at AddEmployee", bex);
                return CreateJsonResponse(Request, null, bex.Code);
            }
            catch (Exception ex)
            {
                //LogError("Unhandled exception occurred.", ex);
                return CreateJsonResponse(Request, null, Globals.MessageCodes.ERROR);
            }
        }

        /// <summary>
        /// Updates existing employee.
        /// </summary>
        /// <param name="employee">Employee object</param>
        /// <returns>HttpResponseMessage with Employee DTO object</returns>
        [Route("employees")]
        [System.Web.Http.HttpPut]
        public HttpResponseMessage UpdateEmployee([FromBody] Employee employee)
        {
            try
            {
                Employee updatedEmployee = EmployeesData.UpdateEmployee(employee);
                return CreateJsonResponse(Request, updatedEmployee, Globals.MessageCodes.SUCCESS);

            }
            catch (BusinessException bex)
            {
                //LogError("Error at GetEmployeeById", bex);
                return CreateJsonResponse(Request, null, bex.Code);
            }
            catch (Exception ex)
            {
                //LogError("Unhandled exception occurred.", ex);
                return CreateJsonResponse(Request, null, Globals.MessageCodes.ERROR);
            }
        }

        /// <summary>
        /// Deletes employee by id.
        /// </summary>
        /// <param name="id">Employee Id</param>
        [Route("employees/{id}")]
        [System.Web.Http.HttpDelete]
        public HttpResponseMessage DeleteEmployeeById([FromUri] int id)
        {
            try
            {
                EmployeesData.DeleteEmployeeById(id);
                return CreateJsonResponse(Request, null, Globals.MessageCodes.SUCCESS);

            }
            catch (BusinessException bex)
            {
                //LogError("Error at DeleteEmployeeById", bex);
                return CreateJsonResponse(Request, null, bex.Code);
            }
            catch (Exception ex)
            {
                //LogError("Unhandled exception occurred.", ex);
                return CreateJsonResponse(Request, null, Globals.MessageCodes.ERROR);
            }
        }

    }
}
