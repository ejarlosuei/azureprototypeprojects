﻿using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Reflection;

namespace EmployeesService
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            string assemblyPath = Assembly.GetExecutingAssembly().Location;
            string employeesControllerPath = assemblyPath.Substring(0, assemblyPath.LastIndexOf("\\")) + "\\Employees.Controller.dll";

            config.Services.Replace(typeof(IAssembliesResolver), new SelfHostAssemblyResolver(employeesControllerPath));

            FormatterConfig.ConfigureFormatters(config.Formatters);

            // Web API routes
            config.MapHttpAttributeRoutes();

        }
    }
}
