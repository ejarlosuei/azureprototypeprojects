﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Reflection;
using System.Web.Http.Dispatcher;

namespace EmployeesService
{
    public class SelfHostAssemblyResolver : IAssembliesResolver
    {
        private string _path = string.Empty;

        public SelfHostAssemblyResolver(string path)
        {
            this._path = path;
        }

        public ICollection<Assembly> GetAssemblies()
        {
            ICollection<Assembly> assemblies = new List<Assembly>();
            assemblies.Add(Assembly.LoadFrom(this._path));
            return assemblies;
        }

    }
}
