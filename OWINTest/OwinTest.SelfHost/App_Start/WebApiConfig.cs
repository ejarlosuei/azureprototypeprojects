﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Reflection;

namespace OwinTest.SelfHost
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            string assemblyPath = Assembly.GetExecutingAssembly().Location;
            string employeesControllerPath = assemblyPath.Substring(0, assemblyPath.LastIndexOf("\\")) + "\\Employees.Controller.dll";

            config.Services.Replace(typeof(IAssembliesResolver), new SelfHostAssemblyResolver(employeesControllerPath));

            // Web API routes
            config.MapHttpAttributeRoutes();



        }
    }
}
