﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NLog;

namespace CommonLayer.Utilities
{
    public class FileLogger : ILogger
    {
        #region Logging

        private static Logger _DefaultLogger = null;
        public static Logger DefaultLogger
        {
            get
            {
                if (_DefaultLogger == null)
                {
                    _DefaultLogger = LogManager.GetLogger("DefaultLogger");
                }
                return _DefaultLogger;
            }
        }

        public void Error(string message, Exception exception = null)
        {
            if (exception == null)
            {
                DefaultLogger.Error(message);
            }
            else
            {
                DefaultLogger.Error(exception, message);
            }
        }

        public void Debug(string message)
        {
            DefaultLogger.Debug(message);
        }

        public void Info(string message)
        {
            DefaultLogger.Info(message);
        }


        #endregion
    }
}
