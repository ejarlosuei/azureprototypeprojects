﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CommonLayer.Web
{
    public static class CORS
    {
        /// <summary>
        /// Sets necessary header values in the HTTP response to allow
        /// Cross Origin Resource Sharing
        /// </summary>
        public static void SetCORSHeaders()
        {
            var res = HttpContext.Current.Response;
            var req = HttpContext.Current.Request;

            // "*" as origin is fine when api is accessed directly from the browser
            //   but request coming from client script requires the real origin 
            string origin = req.Headers["Origin"] == null ? "*" : req.Headers["Origin"];
            res.AppendHeader("Access-Control-Allow-Origin", origin);
            res.AppendHeader("Access-Control-Allow-Credentials", "true");
            res.AppendHeader("Access-Control-Allow-Headers", "Content-Type, X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Date, X-Api-Version, X-File-Name");
            res.AppendHeader("Access-Control-Allow-Methods", "POST,GET,PUT,PATCH,DELETE,OPTIONS");

            // Respond to the AJAX preflight OPTIONS verb
            if (req.HttpMethod == "OPTIONS")
            {
                res.StatusCode = 200;
                res.End();
            }
        }
    }
}
