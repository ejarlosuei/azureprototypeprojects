﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;


namespace CommonLayer.Configuration
{
    public static class AppSettings
    {
        public static bool EnableCORS
        {
            get
            {
                if (!_EnableCORS.HasValue)
                {
                    bool tmp = false;
                    if (ConfigurationManager.AppSettings[""] != null)
                    {
                        bool.TryParse(ConfigurationManager.AppSettings[""], out tmp);
                    }
                    _EnableCORS = tmp;
                }
                return _EnableCORS.Value;
            }

        }
        private static bool? _EnableCORS = null;

    }
}
