﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using BusinessLayer;
using BusinessLayer.DTO;
using BusinessLayer.TestData;

namespace Employees.Controller
{
    public class VersionController : BaseController
    {
        /// <summary>
        /// Retrieves All Employees
        /// </summary>
        /// <returns>HttpResponseMessage with list od Employee DTO objects</returns>
        [Route("version")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetVersionInfo()
        {
            try
            {
                VersionInfo versionInfo = new VersionInfo();

                //versionInfo.nodeName = 

                return CreateJsonResponse(Request, versionInfo, Globals.MessageCodes.SUCCESS);

            }
            catch (BusinessException bex)
            {
                //LogError("Error at GetAllEmployees", bex);
                return CreateJsonResponse(Request, null, bex.Code);
            }
            catch (Exception ex)
            {
                //LogError("Unhandled exception occurred.", ex);
                return CreateJsonResponse(Request, null, Globals.MessageCodes.ERROR);
            }
        }
    }
}
