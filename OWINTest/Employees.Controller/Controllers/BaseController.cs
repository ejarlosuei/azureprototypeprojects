﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Newtonsoft.Json;

using BusinessLayer;
using BusinessLayer.DTO;


namespace Employees.Controller
{
    public class BaseController : ApiController
    {
        public const string JSON_CONTENT_TYPE = "application/json";

        public HttpResponseMessage CreateJsonResponse(HttpRequestMessage request, object contentObject, int messageCode)
        {
            ResponseData responseData = new ResponseData();
            responseData.Code = messageCode;
            responseData.Message = Globals.GetMessage(messageCode);
            responseData.Data = contentObject;

            string serializedContent = JsonConvert.SerializeObject(responseData);
            HttpStatusCode httpStatus = Globals.GetHTTPStatusCode(messageCode);
            HttpResponseMessage response = request.CreateResponse(httpStatus);
            response.Content = new StringContent(serializedContent, Encoding.UTF8, JSON_CONTENT_TYPE);
            return response;
        }

    }
}
