﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BusinessLayer.DTO;

namespace BusinessLayer.TestData
{
    public static class EmployeesData
    {

        private static List<Employee> AllEmployees = new List<Employee>()
        {
           new Employee(){ Id = 1, FirstName = "Edwin", LastName = "Jarlos", Email = "ejarlos@yahoo.com"},
           new Employee(){ Id = 2, FirstName = "John", LastName = "Smith", Email = "jsmith@gmail.com"},
           new Employee(){ Id = 3, FirstName = "Sarrah", LastName = "Connor", Email = "sconnor@gmail.com"}
        };


        /// <summary>
        /// Retreives all employees.
        /// </summary>
        /// <returns>List of Employee objects.</returns>
        public static List<Employee> GetAllEmployees()
        {
            return AllEmployees;
        }

        /// <summary>
        /// Retreives employee by Id.
        /// </summary>
        /// <param name="email">email address</param>
        /// <returns>Employee object</returns>
        public static Employee GetEmployeeById(int id)
        {
            return AllEmployees.Where(x => x.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// Retreives employee by email.
        /// </summary>
        /// <param name="email">email address</param>
        /// <returns>Employee object</returns>
        public static Employee GetEmployeeByEmail(string email)
        {
            Employee retObject = null;

            if (!string.IsNullOrWhiteSpace(email))
            {
                retObject = AllEmployees.Where(x => x.Email.ToLower() == email.ToLower()).FirstOrDefault();
            }
            return retObject;
        }

        /// <summary>
        /// Adds new employee.
        /// </summary>
        /// <param name="employee">Employee object</param>
        /// <returns>Employee object</returns>
        public static Employee AddEmployee(Employee employee)
        {
            Employee retObject = null;

            if(employee != null)
            {
                if(validateEmployee(employee, true))
                {
                    employee.Id = (AllEmployees.Max(x => x.Id) + 1);
                    AllEmployees.Add(employee);
                    retObject = employee;
                }
            }
            return retObject;
        }

        /// <summary>
        /// Updates employee.
        /// </summary>
        /// <param name="employee">Employee object</param>
        /// <returns>Employee object</returns>
        public static Employee UpdateEmployee(Employee employee)
        {
            Employee retObject = null;

            if (employee != null)
            {
                if (validateEmployee(employee, true))
                {
                    Employee currentEmployee = GetEmployeeById(employee.Id);
                    currentEmployee.Email = employee.Email;
                    currentEmployee.FirstName = employee.FirstName;
                    currentEmployee.LastName = employee.LastName;
                    retObject = currentEmployee;
                }
            }
            return retObject;
        }

        /// <summary>
        /// Deletes employee by Id
        /// </summary>
        /// <param name="id">Employee Id</param>
        public static void DeleteEmployeeById(int id)
        {
            Employee employee = GetEmployeeById(id);
            if (employee != null)
            {
                AllEmployees.Remove(employee);
            }
        }


        /// <summary>
        /// Checks for all fields before insert or update
        /// Throws exception if required fields are not present or
        /// unique field already exist.
        /// </summary>
        /// <param name="employee"></param>
        /// <param name="isNew"></param>
        private static bool validateEmployee(Employee employee, bool isNew)
        {
            bool retValue = false;
            if (employee != null)
            {
                if (string.IsNullOrWhiteSpace(employee.Email))
                {
                    throw new BusinessException(Globals.MessageCodes.EMPLOYEE_ERROR_EMAIL_REQUIRED);
                }
                else if (string.IsNullOrWhiteSpace(employee.FirstName))
                {
                    throw new BusinessException(Globals.MessageCodes.EMPLOYEE_ERROR_FIRSTNAME_REQUIRED);
                }
                else if (string.IsNullOrWhiteSpace(employee.LastName))
                {
                    throw new BusinessException(Globals.MessageCodes.EMPLOYEE_ERROR_LASTNAME_REQUIRED);
                }
                else if (isNew)
                {
                    if (GetEmployeeByEmail(employee.Email) != null)
                    {
                        throw new BusinessException(Globals.MessageCodes.EMPLOYEE_ERROR_DUPLICATE);
                    }
                }
                else
                {
                    Employee obj = GetEmployeeById(employee.Id);
                    if(obj == null)
                    {
                        throw new BusinessException(Globals.MessageCodes.EMPLOYEE_ERROR_NOT_FOUND);
                    }
                    else if (obj.Id == GetEmployeeByEmail(employee.Email).Id)
                    {
                        throw new BusinessException(Globals.MessageCodes.EMPLOYEE_ERROR_DUPLICATE);
                    }
                }
                retValue = true;
            }
            return retValue;
        }

    }
}
