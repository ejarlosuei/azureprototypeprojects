﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace BusinessLayer
{
    public class Globals
    {
        /// <summary>
        /// System message codes.
        /// </summary>
        public static class MessageCodes
        {
            // SUCCESS: 0
            public const int SUCCESS = 0;

            // 1000 – 1999: Generic for all systems (can be used by all APIs)
            public const int ERROR = 1000;
            public const int REQUEST_FAIL_BAD_REQUEST = 1001;
            public const int REQUEST_FAIL_UNAUTHORIZED = 1002;
            public const int REQUEST_FAIL_NOT_FOUND = 1003;


            // 2000 -  2100 : Employee
            public const int EMPLOYEE_ERROR_NOT_FOUND = 2000;
            public const int EMPLOYEE_ERROR_DUPLICATE = 2001;
            public const int EMPLOYEE_ERROR_FIRSTNAME_REQUIRED = 2002;
            public const int EMPLOYEE_ERROR_LASTNAME_REQUIRED = 2003;
            public const int EMPLOYEE_ERROR_EMAIL_REQUIRED = 2004;
            
        }

        /// <summary>
        /// Returns the corresponding HTTP Status code given the message code
        /// </summary>
        /// <param name="code">message code</param>
        /// <returns>HttpStatusCode</returns>
        public static HttpStatusCode GetHTTPStatusCode(int code)
        {
            HttpStatusCode httpCode = HttpStatusCode.InternalServerError;
            switch (code)
            {
                case MessageCodes.SUCCESS:
                    httpCode = HttpStatusCode.OK;
                    break;
                case MessageCodes.REQUEST_FAIL_NOT_FOUND:
                    httpCode = HttpStatusCode.NotFound;
                    break;
                case MessageCodes.REQUEST_FAIL_UNAUTHORIZED:
                    httpCode = HttpStatusCode.Unauthorized;
                    break;
                case MessageCodes.REQUEST_FAIL_BAD_REQUEST:
                    httpCode = HttpStatusCode.BadRequest;
                    break;
            }
            return httpCode;
        }

        private static IDictionary<int, string> _ApplicationMessages = new Dictionary<int, string>() {
            // Success
            { MessageCodes.SUCCESS, "Success"},

            // Generic Errors
            { MessageCodes.ERROR, "Unknown Error" },
            { MessageCodes.REQUEST_FAIL_BAD_REQUEST, "Bad Request Error." },
            { MessageCodes.REQUEST_FAIL_UNAUTHORIZED, "User is unauthorized." },
            { MessageCodes.REQUEST_FAIL_NOT_FOUND, "Resource not found." },

            // Employee
            { MessageCodes.EMPLOYEE_ERROR_NOT_FOUND, "Bad API Request Error." },
            { MessageCodes.EMPLOYEE_ERROR_DUPLICATE, "This email already exist." },
            { MessageCodes.EMPLOYEE_ERROR_FIRSTNAME_REQUIRED, "First Name is required." },
            { MessageCodes.EMPLOYEE_ERROR_LASTNAME_REQUIRED, "Last Name is required." },
            { MessageCodes.EMPLOYEE_ERROR_EMAIL_REQUIRED, "Email is required." }            
        };

        /// <summary>
        /// Get the standard application message for the passed code.
        /// Returns null if the code is not found.
        /// </summary>
        /// <param name="code">The code to look up.</param>
        /// <returns>The message for the code, or null if no code found.</returns>
        public static string GetMessage(int code)
        {
            string message = null;
            
            if (_ApplicationMessages.ContainsKey(code))
            {
                message = _ApplicationMessages[code];
            }

            return message;
        }

    }
}
