﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class BusinessException : Exception
    {
        public int Code { get; set; }

        public BusinessException(int code) : base(Globals.GetMessage(code))
        {
            Code = code;
        }

        public BusinessException(int code, string message) : base(message)
        {
            Code = code;
        }
    }

}
