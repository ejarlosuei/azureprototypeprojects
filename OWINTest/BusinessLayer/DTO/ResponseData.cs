﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace BusinessLayer.DTO
{
    public class ResponseData
    {
        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("message")]
        public object Message { get; set; }

        [JsonProperty("data")]
        public object Data { get; set; }
    }
}
