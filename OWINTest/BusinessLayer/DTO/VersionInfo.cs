﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;


namespace BusinessLayer.DTO
{
    public class VersionInfo
    {
        [JsonProperty("businessLayerVersion")]
        public string businessLayerVersion { get; set; }

        [JsonProperty("businessLayerVersion")]
        public string employeesControllerVersion { get; set; }

        [JsonProperty("employeeServiceVersion")]
        public string employeeServiceVersion { get; set; }

        [JsonProperty("nodeName")]
        public string nodeName { get; set; }

        [JsonProperty("ip")]
        public string ip { get; set; }

        [JsonProperty("port")]
        public int port { get; set; }
    }
}
