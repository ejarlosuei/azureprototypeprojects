This example prototype demonstrates hosting of a Web Api in Azure Service Fabric
as compared to hosting it in IIS or Owin selfhost. I have explicitly separated the web api controller so we can easily see the code differences between different hosts.

Requirements to run this Solution:
 - Visual Studio 2015
 - Latest Azure Service Fabric SDK 
 - Azure account for Service Fabric Cluster Creation


EmployeeService
 - This is a stateless web api container which can runs on Local Service Fabric and Azure Service Fabric.
 - To run on Local Service Fabric (assuming it is installed and running in your machine) simply select OwinTest.ServiceFabric as startup project and hit F5.
   Then open your browser and go to http://localhost:20001/employees
 - To publish to Azure Service Fabric please read the instructions below.
 

WebApi.IIS
 - This is a container project that hosts the web api controller in IIS. 
 - To run simply select  WebApi.IIS as Startup project and hit F5.
 
 
OwinTest.IIS
 - This is a container project that hosts the web api controller running on Owin in IIS. 
 - To run simply select OwinTest.IIS as Startup project and hit F5.


OwinTest.SelfHost
 - This is a container project that hosts the web api controller running on self hosted owin. 
 - To run simply select OwinTest.SelfHost as Startup project and hit F5.
   Then open your browser and go to http://localhost:55555/employees


Employees.Controller
 - This is the Web Api controller that is loaded at startup.
 
 
BusinessLayer
 - This is the mock business layer that returns lists of employee data. You can also add, edit and delete employee data.
 

Publishing EmployeeService Web Api to Azure Service Fabric.

1.) Go to Azure Portal and create a new Service Fabric Cluster with the following configuration
    - Initial VM scale capacity = 5
	- Security mode = Unsecure
	- everything else default
	
2.) Once the cluster is created a corresponding Load Balancer is also created for you. Find it and click on it. Its name is usually prefixed with "LB-".
  
3.) When youre in it click on Health probes tab and add a new probe with the following settings
    - Protocol = HTTP
	- Port = 20001
	- everything else default
	
4.) Once the probe is created, you will now create a Load balancing rule with the following settings.
	- Port = 20001
	- Backend Port = 20001
	- everything else default

5.) On Visual Studio right click on OwinTest.ServiceFabric and click publish. 
    - Target profile: PublishProfiles\Cloud.xml
	- Set your Azure account
	- Choose the newly created Service Fabric Cluster as the connection endpoint.
	
6.) You're done! To test open your browser and go to "http://{service fabric cluster}:20001/employees"


