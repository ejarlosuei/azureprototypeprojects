﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Newtonsoft.Json;

using UpgradeService;

namespace UPGX2.Controllers
{
    [RoutePrefix("api/UpgradeService")]
    public class UpgradeServiceController : ApiController
    {

        // GET api/UpgradeService/UpgradeImage?id=T0000&userName=xxx&token=xxx&productName=xxx&ueiRc=xxx&imageType=xxx
        [Route("UpgradeImage")]
        public UpgradeImageResult GetUpgradeImage(string id, string userName, string token, string productName, string ueiRc, int imageType)
        {
            UpgradeService.IdListPickRenamed renamed = new UpgradeService.IdListPickRenamed();
            UpgradeImageResult result = new UpgradeImageResult();

            if (!Authentication.AuthenticateUser(userName, token, productName))
            {
                return result;
            }

            UpgradeService.UpgradeService service = new UpgradeService.UpgradeService();

            byte[] image = null;
            List<string> fromIDList = new List<string>();
            List<string> toIDList = new List<string>();
            List<string> modelList = new List<string>();
            List<string> idList = new List<string>();
            idList.Add(id);


            try
            {
                image = service.GetUpgradeImage(idList, productName, ueiRc, imageType, fromIDList, toIDList, modelList);
            }
            catch (Exception ex)
            {
                result.errorMessage = ex.Message;
            }

            //try
            //{
            //    if (imageType != 4 && imageType != 5 && imageType != 6)
            //    {
            // Eliminate redundant logging
            //        ICAService customerAccess = new CustomerAccess();
            //        customerAccess.LogUpgradeImage(userName, product, idList);
            //    }
            //}
            //catch (Exception ex)
            //{
            //}
            result.image = image;
            renamed.ConvertForService(out result.from, out result.to);
            return result;
        }

    }

    public class UpgradeImageResult
    {
        public int errorCode;
        public string errorMessage;
        public byte[] image;
        public string[] from;
        public string[] to;
    }
}
