﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CAService;
using ProjectSearch;

namespace UPGX2
{
    public static class Authentication
    {
        public static bool AuthenticateUser(string userName, string token, string project)
        {
            string[] projectArray = new string[1];

            projectArray[0] = project;
            return AuthenticateUserMultiple(userName, token, projectArray);
        }

        private static bool AuthenticateUserMultiple(string userName, string token, string[] projectArray)
        {
            CustomerAccess auth = new CustomerAccess();
            CAServiceListEx result = auth.AuthenticateUserDetail(userName, token, token);

            if (result.FullAccess)
                return true;

            Predicate<CAServiceList> isAuthorized = delegate (CAServiceList list)
            {
                foreach (string project in projectArray)
                {
                    if (String.Compare(list.ProjectName, project, true) == 0)
                        return true;
                    if (list.ProductURC != null && list.ProductURC != String.Empty)
                    {
                        string projectName = null;
                        string chipNumber = null;
                        int URCnumber = Convert.ToInt32(list.ProductURC);
                        string revision = null;
                        string description = null;

                        ProjectSelection Fwd = new ProjectSelection();
                        ProjectInfo projectData = Fwd.SelectProject(projectName,
                            chipNumber, URCnumber, revision, description);

                        for (int i = 0; i < projectData.InfoList.Length; i++)
                            if (String.Compare(projectData.InfoList[i].projectName, project, true) == 0)
                                return true;

                        projectData = Fwd.SelectProjectV2Only(projectName,
                            chipNumber, URCnumber, revision, description);

                        for (int i = 0; i < projectData.InfoList.Length; i++)
                            if (String.Compare(projectData.InfoList[i].projectName, project, true) == 0)
                                return true;
                    }
                }
                return false;
            };
            if (projectArray == null || projectArray.Length == 0 || projectArray[0] == null)
                return false;
            if (result.ProjectList != null && Array.Exists(result.ProjectList, isAuthorized))
                return true;
            return false;
        }
    }
}