﻿using System.Data;

namespace CommonStuff.SQL
{
    public class SQLParam
    {
        public SQLParam(string name, object value, SqlDbType dataType)
        {
            Name = name;
            Value = value;
            DataType = dataType;
        }
        public string Name { get; set; }
        public object Value { get; set; }
        public SqlDbType DataType { get; set; }
    }
}
