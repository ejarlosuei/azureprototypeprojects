﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace CommonStuff.SQL
{
    public static class DataAccess
    {
        private const int DEF_CommandTimeoutSeconds = 600; //10 minutes

        public static DataTable GetDataTable(string query, string connectionString, CommandType commandType, List<SQLParam> sqlParameters)
        {
            DataTable dt = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.CommandTimeout = DEF_CommandTimeoutSeconds;
                        cmd.CommandType = commandType;

                        if (sqlParameters != null)
                        {
                            foreach (SQLParam item in sqlParameters)
                            {
                                SqlParameter sqlParameter = new SqlParameter(item.Name, item.DataType);
                                sqlParameter.Value = item.Value;
                                cmd.Parameters.Add(sqlParameter);
                            }
                        }

                        conn.Open();

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            dt = new DataTable();
                            da.Fill(dt);
                        }
                    }
                }
                return dt;
            }
            catch (SqlException ex)
            {
                ex.Data.Add("ConnectionString", connectionString);
                ex.Data.Add("Query", query);
                throw ex;
            }
        }

        public static SqlDataReader GetReader(string query, string connectionString, CommandType commandType, List<SQLParam> sqlParameters)
        {
            SqlDataReader dr = null;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    cmd.CommandTimeout = DEF_CommandTimeoutSeconds;
                    cmd.CommandType = commandType;

                    if (sqlParameters != null)
                    {
                        foreach (SQLParam item in sqlParameters)
                        {
                            SqlParameter sqlParameter = new SqlParameter(item.Name, item.DataType);
                            sqlParameter.Value = item.Value;
                            cmd.Parameters.Add(sqlParameter);
                        }
                    }
                    conn.Open();

                    dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                }
            }

            return dr;

        }

        public static DataSet GetDataSet(string query, string connectionString, CommandType commandType, List<SQLParam> sqlParameters)
        {
            DataSet ds = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.CommandTimeout = DEF_CommandTimeoutSeconds;
                        cmd.CommandType = commandType;

                        if (sqlParameters != null)
                        {
                            foreach (SQLParam item in sqlParameters)
                            {
                                SqlParameter sqlParameter = new SqlParameter(item.Name, item.DataType);
                                sqlParameter.Value = item.Value;
                                cmd.Parameters.Add(sqlParameter);
                            }
                        }
                        conn.Open();

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            ds = new DataSet();
                            da.Fill(ds);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                ex.Data.Add("ConnectionString", connectionString);
                ex.Data.Add("Query", query);
                throw ex;

            }

            return ds;
        }

        public static object ExecuteScalar(string query, string connectionString, CommandType commandType, List<SQLParam> sqlParameters)
        {
            object obj = DBNull.Value;

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.CommandTimeout = DEF_CommandTimeoutSeconds;
                        cmd.CommandType = commandType;

                        if (sqlParameters != null)
                        {
                            foreach (SQLParam item in sqlParameters)
                            {
                                cmd.Parameters.AddWithValue(item.Name, item.Value ?? DBNull.Value);
                            }
                        }
                        conn.Open();
                        obj = cmd.ExecuteScalar();
                    }
                }
            }
            catch (SqlException ex)
            {
                ex.Data.Add("ConnectionString", connectionString);
                ex.Data.Add("Query", query);
                throw ex;
            }

            return obj;
        }

        public static object ExecuteScalarUsingReader(string query, string connectionString, CommandType commandType, List<SQLParam> sqlParameters)
        {
            object obj = DBNull.Value;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    cmd.CommandTimeout = DEF_CommandTimeoutSeconds;
                    cmd.CommandType = commandType;

                    if (sqlParameters != null)
                    {
                        foreach (SQLParam item in sqlParameters)
                        {
                            SqlParameter sqlParameter = new SqlParameter(item.Name, item.DataType);
                            sqlParameter.Value = item.Value;
                            cmd.Parameters.Add(sqlParameter);
                        }
                    }
                    conn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            obj = dr[0];
                            break;
                        }
                    }
                }
            }

            return obj;
        }

        public static int ExecuteNonQuery(string query, string connectionString, CommandType commandType, List<SQLParam> sqlParameters)
        {
            int retVal = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.CommandTimeout = DEF_CommandTimeoutSeconds;
                        cmd.CommandType = commandType;

                        if (sqlParameters != null)
                        {
                            foreach (SQLParam item in sqlParameters)
                            {
                                SqlParameter sqlParameter = new SqlParameter(item.Name, item.DataType);
                                sqlParameter.Value = item.Value;
                                cmd.Parameters.Add(sqlParameter);
                            }
                        }
                        conn.Open();
                        retVal = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException ex)
            {
                ex.Data.Add("ConnectionString", connectionString);
                ex.Data.Add("MethodName", "ExecuteNonQuery");
                throw ex;
            }

            return retVal;
        }

        public static bool BulkInsert(DataTable dt, string tableName, int batchSize, bool keepIdentity, string connectionString)
        {
            bool retVal = false;
            //errorMessage = null;

            using (SqlBulkCopy bulkcopy = new SqlBulkCopy(connectionString, (keepIdentity ? (SqlBulkCopyOptions.KeepIdentity | SqlBulkCopyOptions.TableLock) : SqlBulkCopyOptions.TableLock)))
            {
                bulkcopy.DestinationTableName = tableName;
                bulkcopy.BatchSize = batchSize;
                bulkcopy.BulkCopyTimeout = 0; // 0 is indefinite
                bulkcopy.WriteToServer(dt);

                retVal = true;
            }

            return retVal;
        }

        public static bool ValidateDBConnection(string query, string connectionString, out string msg)
        {
            bool result = false;
            msg = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        object retObject = cmd.ExecuteScalar();
                        if (retObject != null && retObject != DBNull.Value)
                        {
                            result = true;
                        }
                    }
                }

                if (result)
                {
                    msg = string.Format("Successfully connected to '{0}' ", connectionString);
                }
                else
                {
                    msg = string.Format("Could NOT connect to '{0}'", connectionString);
                }

            }
            catch (System.Exception ex) /* EXPLICITLY SUPPRESSED */
            {
                msg = string.Format("ERROR connecting to '{0}' - Exception: {1}", connectionString, ex.StackTrace);
            }
            return result;
        }

        public static string EscapeStringValue(string rawVal)
        {
            string escVal = rawVal;

            if (rawVal != null)
            {
                // Single quotes "'" must be doubled "''".
                if (rawVal.Contains("'"))
                {
                    escVal = rawVal.Replace("'", "''");
                }
            }

            return escVal;
        }

    }
}
