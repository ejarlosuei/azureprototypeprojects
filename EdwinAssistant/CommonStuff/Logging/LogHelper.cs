﻿using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;

using CommonStuff.Config;

namespace CommonStuff.Logging
{
    public static class LogHelper
    {
        /// <summary>
        /// Create Serilog logger based on SEQ. The method reads SEQ settings from IConfiguration object.
        /// </summary>
        /// <param name="applicationName"></param>
        /// <param name="environmentName"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static ILogger CreateSeqLogger(string applicationName, string environmentName, IConfiguration configuration)
        {
           ILogger logger = new LoggerConfiguration()
                .Enrich.WithProperty("AppName", applicationName)
                .Enrich.WithProperty("Environment", environmentName)
                .Enrich.WithMachineName()
                .Enrich.WithEnvironmentUserName()
                .Enrich.FromLogContext()
                .WriteTo.Seq(AppSettings.SeqURL, Serilog.Events.LogEventLevel.Verbose, 5, null, AppSettings.SeqApiKey)
                .CreateLogger();

            return logger;
        }
    }
}
