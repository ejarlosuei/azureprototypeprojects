﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;

namespace CommonStuff.Web
{
    public static class WebHelper
    {
        public enum MethodENUM { GET, POST };
        public enum ContetTypeENUM { JSON, MULTIPART_FORM_DATA, X_WWW_FORM_URLENCODED };

        private const int RequestTimeout = 120000; // 2 mins

        /// <summary>
        /// Web Request Wrapper
        /// </summary>
        /// <param name="method">Http Method</param>
        /// <param name="contentType">Http Content Type</param>
        /// <param name="url">Full url of the web resource</param>
        /// <param name="postData">Data to post</param>
        /// <returns>The web server response.</returns>
        public static string WebRequest(MethodENUM method, ContetTypeENUM contentType, string url, string postData=null)
        {
            HttpWebRequest webRequest = null;
            string responseData = string.Empty;

            webRequest = System.Net.WebRequest.Create(url) as HttpWebRequest;
            webRequest.Method = method.ToString();
            webRequest.ServicePoint.Expect100Continue = false;
            webRequest.Timeout = RequestTimeout;
            
            switch (contentType)
            {
                case ContetTypeENUM.X_WWW_FORM_URLENCODED:
                    webRequest.ContentType = "application/x-www-form-urlencoded";
                    break;
                case ContetTypeENUM.MULTIPART_FORM_DATA:
                    webRequest.ContentType = "multipart/form-data";
                    break;
                case ContetTypeENUM.JSON:
                default:
                    webRequest.ContentType = "application/json";
                    break;
            }


            if (method == MethodENUM.POST && !string.IsNullOrWhiteSpace(postData))
            {

                using (Stream requestStream = webRequest.GetRequestStream())
                {
                    using (StreamWriter requestWriter = new StreamWriter(requestStream))
                    {
                        requestWriter.Write(postData);
                    }
                }
            }

            responseData = WebResponseGet(webRequest);
            webRequest = null;
            return responseData;
        }

        /// <summary>
        /// Process the web response.
        /// </summary>
        /// <param name="webRequest">The request object.</param>
        /// <returns>The response data.</returns>
        private static string WebResponseGet(HttpWebRequest webRequest)
        {
            string responseData = string.Empty;

            using (Stream responseStream = webRequest.GetResponse().GetResponseStream())
            {
                using (StreamReader responseReader = new StreamReader(responseStream))
                {
                    responseData = responseReader.ReadToEnd();
                }
            }

            return responseData;
        }
    }
}
