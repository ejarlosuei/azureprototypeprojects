﻿namespace CommonStuff.Web
{
    public class ResponseObject
    {
        public int Code { get; set; }
        public string ErrorMessage { get; set; }
        public object Data { get; set; }
    }
}
