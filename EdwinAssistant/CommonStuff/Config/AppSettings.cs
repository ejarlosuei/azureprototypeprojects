﻿using System;
using System.Collections.Generic;
using System.Configuration;

using Microsoft.Extensions.Configuration;


namespace CommonStuff.Config
{
    public static class AppSettings
    {
        private static IConfiguration Configuration { get; set; }

        public static void InitConfig(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        #region Seq

        public static string SeqURL
        {
            get
            {
                if (_SeqURL == null)
                {
                    _SeqURL = Configuration["Seq:URL"];
                }
                return _SeqURL;
            }
        }
        private static string _SeqURL = null;

        public static string SeqApiKey
        {
            get
            {
                if (_SeqApiKey == null)
                {
                    _SeqApiKey = Configuration["Seq:ApiKey"];
                }
                return _SeqApiKey;
            }
        }
        private static string _SeqApiKey = null;

        #endregion

    }
}
