﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Integration.AspNet.Core;
using Microsoft.Extensions.Logging;
using Serilog;

namespace EdwinAssistant.Controllers
{
    [Route("api/messages")]
    [ApiController]
    public class BotController : ControllerBase
    {
        private readonly IBotFrameworkHttpAdapter _adapter;
        private readonly IBot _bot;
        private readonly ILogger<BotController> _logger;

        public BotController(IBotFrameworkHttpAdapter httpAdapter, IBot bot, ILogger<BotController> logger)
        {
            _adapter = httpAdapter;
            _bot = bot;
            _logger = logger;
        }

        [HttpPost]
        [HttpGet]
        public async Task PostAsync()
        {
            _logger.LogInformation($"Request received from: {Request.Host.Host}");
            await _adapter.ProcessAsync(Request, Response, _bot);
        }
    }
}