﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
using System;
using System.Diagnostics;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

using CommonStuff.Config;
using CommonStuff.Logging;


namespace EdwinAssistant
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //InitConfigSettings();
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>() // Note: Application Insights is added in Startup.  Disabling is also handled there.
                .UseSerilog()
                .Build();

        private static void InitConfigSettings()
        {
            string environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var builder = new ConfigurationBuilder();
            builder.AddJsonFile($"appsettings.json", false, true);

            IConfiguration config = builder.Build();
            AppSettings.InitConfig(config);
            Process process = Process.GetCurrentProcess();

            Log.Logger = LogHelper.CreateSeqLogger(process.ProcessName, environmentName, config);
        }

    }
}
