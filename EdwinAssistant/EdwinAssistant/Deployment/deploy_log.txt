{
  "error": null,
  "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Resources/deployments/deployment_dry_run",
  "name": "deployment_dry_run",
  "properties": {
    "correlationId": "6b6fca7d-a9c2-4445-8bd1-eff2aaf767e9",
    "debugSetting": null,
    "dependencies": [
      {
        "dependsOn": [
          {
            "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Web/serverfarms/EdwinBot-xpys4my",
            "resourceGroup": "EdwinBot",
            "resourceName": "EdwinBot-xpys4my",
            "resourceType": "Microsoft.Web/serverfarms"
          }
        ],
        "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Web/sites/EdwinBot-xpys4my",
        "resourceGroup": "EdwinBot",
        "resourceName": "EdwinBot-xpys4my",
        "resourceType": "Microsoft.Web/sites"
      },
      {
        "dependsOn": [
          {
            "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Insights/components/EdwinBot-xpys4my",
            "resourceGroup": "EdwinBot",
            "resourceName": "EdwinBot-xpys4my",
            "resourceType": "Microsoft.Insights/components"
          }
        ],
        "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.BotService/botServices/EdwinBot-xpys4my",
        "resourceGroup": "EdwinBot",
        "resourceName": "EdwinBot-xpys4my",
        "resourceType": "Microsoft.BotService/botServices"
      },
      {
        "dependsOn": [
          {
            "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Web/Sites/EdwinBot-qnahost-xpys4my",
            "resourceGroup": "EdwinBot",
            "resourceName": "EdwinBot-qnahost-xpys4my",
            "resourceType": "Microsoft.Web/Sites"
          },
          {
            "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Search/searchServices/edwinbot-search-xpys4my",
            "resourceGroup": "EdwinBot",
            "resourceName": "edwinbot-search-xpys4my",
            "resourceType": "Microsoft.Search/searchServices"
          },
          {
            "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/microsoft.insights/components/EdwinBot-xpys4my",
            "resourceGroup": "EdwinBot",
            "resourceName": "EdwinBot-xpys4my",
            "resourceType": "microsoft.insights/components"
          }
        ],
        "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.CognitiveServices/accounts/EdwinBot-qna-xpys4my",
        "resourceGroup": "EdwinBot",
        "resourceName": "EdwinBot-qna-xpys4my",
        "resourceType": "Microsoft.CognitiveServices/accounts"
      },
      {
        "dependsOn": [
          {
            "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Web/serverFarms/EdwinBot-xpys4my",
            "resourceGroup": "EdwinBot",
            "resourceName": "EdwinBot-xpys4my",
            "resourceType": "Microsoft.Web/serverFarms"
          }
        ],
        "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Web/sites/EdwinBot-qnahost-xpys4my",
        "resourceGroup": "EdwinBot",
        "resourceName": "EdwinBot-qnahost-xpys4my",
        "resourceType": "Microsoft.Web/sites"
      },
      {
        "dependsOn": [
          {
            "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Web/Sites/EdwinBot-qnahost-xpys4my",
            "resourceGroup": "EdwinBot",
            "resourceName": "EdwinBot-qnahost-xpys4my",
            "resourceType": "Microsoft.Web/Sites"
          },
          {
            "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Insights/components/EdwinBot-xpys4my",
            "resourceGroup": "EdwinBot",
            "resourceName": "EdwinBot-xpys4my",
            "resourceType": "Microsoft.Insights/components"
          },
          {
            "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Search/searchServices/edwinbot-search-xpys4my",
            "resourceGroup": "EdwinBot",
            "resourceName": "edwinbot-search-xpys4my",
            "resourceType": "Microsoft.Search/searchServices"
          },
          {
            "actionName": "listAdminKeys",
            "apiVersion": "2015-08-19",
            "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Search/searchServices/edwinbot-search-xpys4my",
            "resourceGroup": "EdwinBot",
            "resourceName": "edwinbot-search-xpys4my",
            "resourceType": "Microsoft.Search/searchServices"
          },
          {
            "apiVersion": "2015-05-01",
            "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Insights/components/EdwinBot-xpys4my",
            "resourceGroup": "EdwinBot",
            "resourceName": "EdwinBot-xpys4my",
            "resourceType": "Microsoft.Insights/components"
          }
        ],
        "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Web/sites/EdwinBot-qnahost-xpys4my/config/appsettings",
        "resourceGroup": "EdwinBot",
        "resourceName": "EdwinBot-qnahost-xpys4my/appsettings",
        "resourceType": "Microsoft.Web/sites/config"
      }
    ],
    "duration": "PT0S",
    "mode": "Incremental",
    "onErrorDeployment": null,
    "outputs": null,
    "parameters": {
      "appInsightsLocation": {
        "type": "String",
        "value": "westus"
      },
      "appInsightsName": {
        "type": "String",
        "value": "EdwinBot-xpys4my"
      },
      "appServicePlanName": {
        "type": "String",
        "value": "EdwinBot-xpys4my"
      },
      "appServicePlanSku": {
        "type": "Object",
        "value": {
          "name": "S1",
          "tier": "Standard"
        }
      },
      "botServiceName": {
        "type": "String",
        "value": "EdwinBot-xpys4my"
      },
      "botServiceSku": {
        "type": "String",
        "value": "S1"
      },
      "botWebAppName": {
        "type": "String",
        "value": "EdwinBot-xpys4my"
      },
      "contentModeratorLocation": {
        "type": "String",
        "value": "westus"
      },
      "contentModeratorName": {
        "type": "String",
        "value": "EdwinBot-cm-xpys4my"
      },
      "contentModeratorSku": {
        "type": "String",
        "value": "S0"
      },
      "cosmosDbName": {
        "type": "String",
        "value": "EdwinBot-xpys4my"
      },
      "location": {
        "type": "String",
        "value": "westus"
      },
      "luisServiceLocation": {
        "type": "String",
        "value": "westus"
      },
      "luisServiceName": {
        "type": "String",
        "value": "EdwinBot-luis-xpys4my"
      },
      "luisServiceSku": {
        "type": "String",
        "value": "S0"
      },
      "microsoftAppId": {
        "type": "String",
        "value": "098b537e-6a52-47e8-a4ec-aad126384ec1"
      },
      "microsoftAppPassword": {
        "type": "String",
        "value": "TheQuickBrownFox!1"
      },
      "name": {
        "type": "String",
        "value": "EdwinBot"
      },
      "qnaMakerSearchName": {
        "type": "String",
        "value": "EdwinBot-search-xpys4my"
      },
      "qnaMakerSearchSku": {
        "type": "String",
        "value": "standard"
      },
      "qnaMakerServiceName": {
        "type": "String",
        "value": "EdwinBot-qna-xpys4my"
      },
      "qnaMakerServiceSku": {
        "type": "String",
        "value": "S0"
      },
      "qnaMakerWebAppName": {
        "type": "String",
        "value": "EdwinBot-qnahost-xpys4my"
      },
      "qnaServiceLocation": {
        "type": "String",
        "value": "westus"
      },
      "storageAccountName": {
        "type": "String",
        "value": "EdwinBot-xpys4my"
      },
      "suffix": {
        "type": "String",
        "value": "xpys4my"
      },
      "useContentModerator": {
        "type": "Bool",
        "value": true
      },
      "useCosmosDb": {
        "type": "Bool",
        "value": true
      },
      "useStorage": {
        "type": "Bool",
        "value": true
      }
    },
    "parametersLink": null,
    "providers": [
      {
        "id": null,
        "namespace": "Microsoft.Resources",
        "registrationPolicy": null,
        "registrationState": null,
        "resourceTypes": [
          {
            "aliases": null,
            "apiVersions": null,
            "capabilities": null,
            "locations": [
              null
            ],
            "properties": null,
            "resourceType": "deployments"
          }
        ]
      },
      {
        "id": null,
        "namespace": "Microsoft.DocumentDB",
        "registrationPolicy": null,
        "registrationState": null,
        "resourceTypes": [
          {
            "aliases": null,
            "apiVersions": null,
            "capabilities": null,
            "locations": [
              "westus"
            ],
            "properties": null,
            "resourceType": "databaseAccounts"
          }
        ]
      },
      {
        "id": null,
        "namespace": "Microsoft.Storage",
        "registrationPolicy": null,
        "registrationState": null,
        "resourceTypes": [
          {
            "aliases": null,
            "apiVersions": null,
            "capabilities": null,
            "locations": [
              "westus"
            ],
            "properties": null,
            "resourceType": "storageAccounts"
          }
        ]
      },
      {
        "id": null,
        "namespace": "Microsoft.Web",
        "registrationPolicy": null,
        "registrationState": null,
        "resourceTypes": [
          {
            "aliases": null,
            "apiVersions": null,
            "capabilities": null,
            "locations": [
              "westus"
            ],
            "properties": null,
            "resourceType": "serverFarms"
          },
          {
            "aliases": null,
            "apiVersions": null,
            "capabilities": null,
            "locations": [
              "westus"
            ],
            "properties": null,
            "resourceType": "sites"
          },
          {
            "aliases": null,
            "apiVersions": null,
            "capabilities": null,
            "locations": [
              null
            ],
            "properties": null,
            "resourceType": "sites/config"
          }
        ]
      },
      {
        "id": null,
        "namespace": "Microsoft.Insights",
        "registrationPolicy": null,
        "registrationState": null,
        "resourceTypes": [
          {
            "aliases": null,
            "apiVersions": null,
            "capabilities": null,
            "locations": [
              "westus"
            ],
            "properties": null,
            "resourceType": "components"
          }
        ]
      },
      {
        "id": null,
        "namespace": "Microsoft.BotService",
        "registrationPolicy": null,
        "registrationState": null,
        "resourceTypes": [
          {
            "aliases": null,
            "apiVersions": null,
            "capabilities": null,
            "locations": [
              "global"
            ],
            "properties": null,
            "resourceType": "botServices"
          }
        ]
      },
      {
        "id": null,
        "namespace": "Microsoft.CognitiveServices",
        "registrationPolicy": null,
        "registrationState": null,
        "resourceTypes": [
          {
            "aliases": null,
            "apiVersions": null,
            "capabilities": null,
            "locations": [
              "westus"
            ],
            "properties": null,
            "resourceType": "accounts"
          }
        ]
      },
      {
        "id": null,
        "namespace": "Microsoft.Search",
        "registrationPolicy": null,
        "registrationState": null,
        "resourceTypes": [
          {
            "aliases": null,
            "apiVersions": null,
            "capabilities": null,
            "locations": [
              "westus"
            ],
            "properties": null,
            "resourceType": "searchServices"
          }
        ]
      }
    ],
    "provisioningState": "Succeeded",
    "template": null,
    "templateHash": "8448749211256882724",
    "templateLink": null,
    "timestamp": "2019-11-19T19:05:17.692401+00:00",
    "validatedResources": [
      {
        "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Resources/deployments/3822b5f4-d098-4b57-9ee6-3bee686aec4c",
        "resourceGroup": "EdwinBot"
      },
      {
        "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.DocumentDB/databaseAccounts/edwinbot-xpys4my",
        "resourceGroup": "EdwinBot"
      },
      {
        "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Storage/storageAccounts/edwinbotxpys4my",
        "resourceGroup": "EdwinBot"
      },
      {
        "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Web/serverFarms/EdwinBot-xpys4my",
        "resourceGroup": "EdwinBot"
      },
      {
        "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Insights/components/EdwinBot-xpys4my",
        "resourceGroup": "EdwinBot"
      },
      {
        "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Web/sites/EdwinBot-xpys4my",
        "resourceGroup": "EdwinBot"
      },
      {
        "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.BotService/botServices/EdwinBot-xpys4my",
        "resourceGroup": "EdwinBot"
      },
      {
        "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.CognitiveServices/accounts/EdwinBot-cm-xpys4my",
        "resourceGroup": "EdwinBot"
      },
      {
        "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.CognitiveServices/accounts/EdwinBot-luis-xpys4my",
        "resourceGroup": "EdwinBot"
      },
      {
        "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.CognitiveServices/accounts/EdwinBot-qna-xpys4my",
        "resourceGroup": "EdwinBot"
      },
      {
        "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Search/searchServices/edwinbot-search-xpys4my",
        "resourceGroup": "EdwinBot"
      },
      {
        "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Web/sites/EdwinBot-qnahost-xpys4my",
        "resourceGroup": "EdwinBot"
      },
      {
        "id": "/subscriptions/6ac2ddc8-6f1c-475f-a8f9-c03b5f3697f0/resourceGroups/EdwinBot/providers/Microsoft.Web/sites/EdwinBot-qnahost-xpys4my/config/appsettings",
        "resourceGroup": "EdwinBot"
      }
    ]
  },
  "resourceGroup": "EdwinBot",
  "type": "Microsoft.Resources/deployments"
}
{
  "applicationInsights": {
    "type": "Object",
    "value": {
      "InstrumentationKey": "d4bdc458-bf47-46f5-99e8-83493696edaf"
    }
  },
  "blobStorage": {
    "type": "Object",
    "value": {
      "connectionString": "DefaultEndpointsProtocol=https;AccountName=edwinbotxpys4my;AccountKey=dfXpEIgvAq32j4Ke6BukAb+4qWxHmGG6FB7zLj6dcx3QeXs4llgCrDxqoCIUk4Hi1V7zGEb7k39iYVlelljSyQ==;EndpointSuffix=core.windows.net",
      "container": "transcripts"
    }
  },
  "botWebAppName": {
    "type": "String",
    "value": "EdwinBot-xpys4my"
  },
  "contentModerator": {
    "type": "Object",
    "value": {
      "key": "d11f3bc62983491e8820375addb049f2"
    }
  },
  "cosmosDb": {
    "type": "Object",
    "value": {
      "authKey": "AQZbfPAFCCTODAPASKCdvGeDQn6xG658k9yKh4Tj3g4f9ZsCSB6n5eX2OsB1MHd3BLxIqtcMTPrcVTUs1ibnaw==",
      "collectionId": "botstate-collection",
      "cosmosDBEndpoint": "https://edwinbot-xpys4my.documents.azure.com:443/",
      "databaseId": "botstate-db"
    }
  },
  "luis": {
    "type": "Object",
    "value": {
      "accountName": "EdwinBot-luis-xpys4my",
      "key": "7f41acf5afa746fbb466e5b3e715ad1d",
      "region": "westus"
    }
  },
  "qnaMaker": {
    "type": "Object",
    "value": {
      "endpoint": "https://edwinbot-qnahost-xpys4my.azurewebsites.net",
      "key": "2c5c483b415c40fa9f3034b23493de4b"
    }
  },
  "resourceGroupName": {
    "type": "String",
    "value": "EdwinBot"
  }
}
